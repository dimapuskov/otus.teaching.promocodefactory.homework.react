import "regenerator-runtime/runtime";
const axios = require("axios");
const MockAdapter = require("axios-mock-adapter");

const mock = new MockAdapter(axios);

mock.onPost("/api/login").reply(201, {answer: "success"})

export const loginQuery = (user) => {
    return axios.post(`/api/login`, user).then(resp=>resp.data).catch(error=>console.log(error));
};

