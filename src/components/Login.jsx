import React from "react";
import {loginQuery} from "../utils";
import "./Login.css";


class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state =
            {
                login: "",
                password: ""
            };
    }
    render() {
        return(
            <div className={"auth-form-body mt-3"}>
                <h1>Sign in</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <b>Login</b>
                    </label>
                    <input placeholder={"Enter email"} type={"email"} name={"login"} value={this.state.login} onChange={this.handleChange}/>
                    <label>
                        <b>Password</b>
                    </label>
                    <input placeholder={"Enter password"} type={"password"} name={"password"} value={this.state.password} onChange={this.handleChange} />
                    <br/>
                    <button type="submit" >Sign in</button>
                </form>
            </div>
        );
    }

    handleSubmit = event => {
        event.preventDefault();
        console.log("asd");
        const user ={
          login: this.state.login,
          password: this.state.password
        };
        loginQuery(user).then(resp=>console.log(resp));
    }

    handleChange = event =>{
        this.setState({[event.target.name] : event.target.value});
    }
}

export default Login;