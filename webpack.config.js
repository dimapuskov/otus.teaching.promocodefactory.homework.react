const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: "development",

    context: path.resolve(__dirname, 'src'),
    entry: {
        main: './index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].[hash].js",
        publicPath: '/'
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: `./index.html`
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin(),
    ],
    resolve: {
        extensions: ['', '.js', '.jsx', '.css'],

    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                        ]
                    }
                }
            },
            {
                test: /\.s[ac]ss$/,
                use:
                    [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {}
                        },

                        "sass-loader"
                    ]
            },
            {
                test: /\.(ttf|woff|woff2|png|jpe?g|svg|gif|ico)$/,
                type: 'asset/resource',
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'dist'),
        },

        compress: true,
        port: 3300,
        historyApiFallback: true,
    }
};